data "aws_vpc" "vpc" {
  id = var.VPC_ID
}

data "aws_vpc" "mgmt" {
  id = var.MANAGEMENT_VPC_ID
}