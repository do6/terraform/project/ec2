output "SG_SSH_INTERNAL" {
  value = aws_security_group.ssh_sg_tf.id
}
output "SG_CART_SERVICE" {
  value = aws_security_group.cart_sg_tf.id
}
output "SG_CATALOGUE_SERVICE" {
  value = aws_security_group.catalogue_sg_tf.id
}