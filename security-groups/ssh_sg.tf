resource "aws_security_group" "ssh_sg_tf" {
  name        = "allow_ssh_traffice"
  description = "Allow ssh traffic"
  vpc_id      = var.VPC_ID

  ingress {
    description = "TLS from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [data.aws_vpc.vpc.cidr_block,data.aws_vpc.mgmt.cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_ssh_traffice"
  }
}