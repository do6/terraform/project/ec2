resource "aws_instance" "web" {
  count                   = 1
  ami                     = data.aws_ami.ami.id
  instance_type           = var.INSTANCE_TYPE
  key_name                = var.KEY_NAME
  vpc_security_group_ids  = [var.SG_CATALOGUE_SERVICE]
  subnet_id               = element(var.PRIVATE_SUBNETS_IDS, count.index)

  tags = {
    Name = local.Instance-name
  }
}