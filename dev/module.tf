module "SG" {
  source                  = "../security-groups"
  MANAGEMENT_VPC_ID       = data.terraform_remote_state.vpc.outputs.MANAGEMENT_VPC_ID
  VPC_ID                  = data.terraform_remote_state.vpc.outputs.VPC_ID
}
module "ec2-cart" {
  source                  = "../cart"
  INSTANCE_TYPE           = var.INSTANCE_TYPE
  KEY_NAME                = var.KEY_NAME
  MANAGEMENT_VPC_ID       = data.terraform_remote_state.vpc.outputs.MANAGEMENT_VPC_ID
  PRIVATE_SUBNETS_IDS     = data.terraform_remote_state.vpc.outputs.PRIVATE_SUBNETS_IDS
  PUBLIC_SUBNTS_IDS       = data.terraform_remote_state.vpc.outputs.PUBLIC_SUBNTS_IDS
  VPC_ID                  = data.terraform_remote_state.vpc.outputs.VPC_ID
  TAGS                    = var.TAGS
  SG_SSH_INTERNAL         = module.SG.SG_SSH_INTERNAL
  SG_CART_SERVICE         = module.SG.SG_CART_SERVICE
}

module "ec2-catalogue" {
  source                  = "../catalogue"
  INSTANCE_TYPE           = var.INSTANCE_TYPE
  KEY_NAME                = var.KEY_NAME
  MANAGEMENT_VPC_ID       = data.terraform_remote_state.vpc.outputs.MANAGEMENT_VPC_ID
  PRIVATE_SUBNETS_IDS     = data.terraform_remote_state.vpc.outputs.PRIVATE_SUBNETS_IDS
  PUBLIC_SUBNTS_IDS       = data.terraform_remote_state.vpc.outputs.PUBLIC_SUBNTS_IDS
  VPC_ID                  = data.terraform_remote_state.vpc.outputs.VPC_ID
  TAGS                    = var.TAGS
  SG_SSH_INTERNAL         = module.SG.SG_SSH_INTERNAL
  SG_CATALOGUE_SERVICE    = module.SG.SG_CATALOGUE_SERVICE
}



