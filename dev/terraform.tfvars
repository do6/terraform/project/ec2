INSTANCE_TYPE     =   "t2.micro"
KEY_NAME          =   "devops"
TAGS              =   {
  PROJECT         = "Robot-Shop"
  ENV             = "Dev"
  GIT_REPO        = "ok"
  IAC             = "Terraform"
}