data "aws_ami" "ami" {
  most_recent      = true
  name_regex       = "^Centos-7-DevOps-Practice"
  owners           = ["self"]

}
data "aws_vpc" "vpc_id" {
  id = var.VPC_ID
}

data "aws_vpc" "management_vpc_id" {
  id = var.MANAGEMENT_VPC_ID
}